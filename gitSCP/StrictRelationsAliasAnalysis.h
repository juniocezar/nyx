#ifndef __StrictRelationsAliasAnalysis_H__
#define __StrictRelationsAliasAnalysis_H__

#include "llvm/ADT/SparseBitVector.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Pass.h"

/*#include "../RangeAnalysis/RangeAnalysis.h"*/ //REM: Original RA include line
#include "../src/RangeAnalysis/RangeAnalysis.h"

#include <queue>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <map>
#include <utility>
#include <iterator> 

#include <sstream>  // REM: Required include for ..
#include <fstream>  // REM: Required include for ..
#include <list>     // REM: Required include for ..

typedef InterProceduralRA<Cousot> InterProceduralRACousot;

namespace {

////////////////////////////////////////////////////////////////////////////////
// Representation of types as sequences of primitive values (now bits!)
class Primitives {
  public:
  //Holds a Primitive Layout for a determined Type
  struct PrimitiveLayout {
    Type * type;
    std::vector<int> layout;
    PrimitiveLayout(Type* ty, std::vector<int> lay) {
      type = ty;
      layout = lay;
    }
  };
  struct NumPrimitive {
    Type * type;
    int num;
    NumPrimitive(Type* ty, int n) {
      type = ty;
      num = n;
    }
  };
  std::vector<PrimitiveLayout*> PrimitiveLayouts;
  std::vector<NumPrimitive*> NumPrimitives;
  std::vector<int> getPrimitiveLayout(Type* type);
  int getNumPrimitives(Type* type);
  llvm::Type* getTypeInside(Type* type, int i);
  int getSumBehind(std::vector<int> v, unsigned int i);
};
////////////////////////////////////////////////////////////////////////////////

//Forward declarations
class WorkListEngine;
class Constraint;

void* global_variable_translator;

template <class V> class BitVectorPositionTranslator {
  std::unordered_map<V, int> v_to_i;
  std::unordered_map<int, V> i_to_v;
  unsigned next_i;
  
  public:
  bool addValue(V v) {
    if(!v_to_i.count(v)) {
      v_to_i[v] = next_i;
      i_to_v[next_i] = v;
      ++next_i;
      return true;
    } else {
      return false;
    }
  }
  
  unsigned getPosition(V v) {
    if(!(v_to_i.count(v))) addValue(v);
    return v_to_i[v];
  }
  
  V getValue(unsigned i) {
    if(i_to_v.count(i)) return i_to_v[i];
    else return NULL;
  }
    
};

class StrictRelations : public ModulePass, public AliasAnalysis {

public:
  ~StrictRelations();
  static char ID; // Class identification, replacement for typeinfo
  StrictRelations() : ModulePass(ID){}

  /// getAdjustedAnalysisPointer - This method is used when a pass implements
  /// an analysis interface through multiple inheritance.  If needed, it
  /// should override this to adjust the this pointer as needed for the
  /// specified pass info.
  virtual void *getAdjustedAnalysisPointer(AnalysisID PI) override {
    if (PI == &AliasAnalysis::ID)
      return (AliasAnalysis*)this;
    return this;
  }
  
  struct Variable;
  
  class VariableSet {
    friend class VariableSetIterator;
    
    BitVectorPositionTranslator<Variable*>* trans;
    SparseBitVector<> set;
    unsigned int size;
    
    class VariableSetIterator { 
      SparseBitVector<>::iterator it;
      VariableSet* owner;
      public:
      // Preincrement.
      inline VariableSetIterator& operator++() {
        ++it;
        return *this;
      }
   
      // Postincrement.
      inline VariableSetIterator operator++(int) {
        VariableSetIterator tmp = *this;
        ++*this;
        return tmp;
      }
   
      // Return the current set bit number.
      Variable* operator*() const {
        return owner->trans->getValue(*it);
      }
   
      bool operator==(const VariableSetIterator &RHS) const {
        return it == RHS.it;
      }
   
      bool operator!=(const VariableSetIterator &RHS) const {
        return it != RHS.it;
      }
   
      VariableSetIterator() {
      }
   
      VariableSetIterator(SparseBitVector<>::iterator It, VariableSet* Owner){
        it = It;
        owner = Owner;
      }
    };
    
    public:
    typedef VariableSetIterator iterator;
    
    void insert(Variable* v) {
      trans->addValue(v);
      set.set(trans->getPosition(v));
    }
    int count(Variable* v) {
      trans->addValue(v);
      if(set.test(trans->getPosition(v))) return 1;
      else return 0; 
    }
    
    iterator begin() {
      return iterator(set.begin(), this);
    }
    
    iterator end() {
      return iterator(set.end(), this);
    }
    
    void erase(Variable* v) {
      trans->addValue(v);
      set.reset(trans->getPosition(v));
    }
    
    VariableSet() {
      trans = (BitVectorPositionTranslator<Variable*>*) global_variable_translator;
    }
        
  };
  
  struct Variable {
    const Value* v;
    VariableSet LT;
    VariableSet GT;
    VariableSet MA; //REM: MustAlias set
    std::unordered_set<Constraint*> constraints;
    Variable(const Value* V) : v(V) { MA.insert(this); } // REM: Initializating in MA set
    
    void printStrictRelations(raw_ostream &OS);
  };
     
  //Forward declarations
  class DepEdge;

  struct DepNode {
    const Value* v;
    std::unordered_set<DepEdge*> inedges;
    std::unordered_set<DepEdge*> outedges;
    //Types
    bool arg;
    bool unk;
    bool global;
    bool alloca;
    bool call;
    std::unordered_set<const Value*> locs;
    
    DepNode(const Value* V) : v(V) {
      arg = false; unk = false; global = false; call = false;
      mustalias = new std::unordered_set<DepNode*>();
      mustalias->insert(this);
    }
    
    static void addEdge(DepNode* in, DepNode* out, Range r){
      DepEdge* e = new DepEdge(in, out, r);
      in->inedges.insert(e);
      out->outedges.insert(e);  
    }
    static void addEdge(DepNode* in, DepNode* out, Range r, const Value* o) {
      DepEdge* e = new DepEdge(in, out, r, o);
      in->inedges.insert(e);
      out->outedges.insert(e); 
    }
  
    // function and structures for the local analysis
    DepNode *local_root;
    std::map< DepNode*, std::pair<int, Range> > path_to_root;
    void getPathToRoot();
    
    // must alias information
    std::unordered_set<DepNode*>* mustalias;
    void coalesce (DepNode*);
    
  };
  
  struct DepEdge {
    DepNode* const  in;
    DepNode* const out;
    const Range range;
    const Value* const offset;
    
    DepEdge(DepNode* In, DepNode* Out, const Range R, 
    const Value* Offset = NULL) : 
      in(In), out(Out), range(R), offset(Offset) { }
      
    static void deleteEdge(DepEdge* e){
      e->in->inedges.erase(e);
      e->out->outedges.erase(e);
      delete e;  
    }
  };


  Variable* getVariable(Value * V) {
    if(variables.count(V)) return variables.at(V);
    else return NULL;
  }
  void printAllStrictRelations(raw_ostream &OS);

  std::unordered_map<const Value*, Variable*> getResult(){ //REM: Get result
    return variables;
  }
 

  InterProceduralRACousot *RA;
  std::unordered_map<const Value*, Variable*> variables;
  std::unordered_map<const Value*, DepNode*> nodes;
  WorkListEngine* wle;
            
  void getAnalysisUsage(AnalysisUsage &AU) const override;
  
  AliasResult alias(const MemoryLocation &LocA,
                              const MemoryLocation &LocB) override;
  bool runOnModule(Module &M) override;

private:  

  bool aliastest1(const Value* p1, const Value* p2);
  bool aliastest2(const Value* p1, const Value* p2);
  bool aliastest3(const Value* p1, const Value* p2);
  
  enum CompareResult {L, G, E, N};
  CompareResult compareValues(const Value*, const Value*);
  bool disjointGEPs(const GetElementPtrInst*, const GetElementPtrInst*);
  
  // Phases
  Range processGEP(const Value*, const Use*, const Use*);
  void collectConstraintsFromModule(Module &M);
  void buildDepGraph(Module &M);
  void collectTypes();
  void propagateTypes();
  void propagateArgs(std::set<DepNode*> &args);
  void propagateGlobals(std::set<DepNode*> &globals);
  void propagateUnks(std::set<DepNode*> &unks);
  void propagateAlloca(DepNode*);

  //Times
  float phase1;
  float phase2;
  float phase3;
  float phases;
  float test1;
  float test2;
  float test3;

  static Primitives P;
};
////////////////////////////////////////////////////////////////////////////////

//Worklist engine declarations
class Constraint;

class WorkListEngine {
public:
  void solve();
  void add(const Constraint*);
  void push(const Constraint*);
  void printConstraints(raw_ostream &OS);
  std::unordered_map<const Constraint*, bool> getConstraints() { return constraints; }
  int getNumConstraints() { return constraints.size(); }

  //WorkListEngine is in charge of constraint deletion
  ~WorkListEngine();
  
private:
  std::queue<const Constraint*> worklist;
  std::unordered_map<const Constraint*, bool> constraints;
};

class Constraint {
public:
  enum ConstraintKind { //REM: Discriminator for LLVM-style RTTI - users dyn_cast isa
     CK_LT,
     CK_LE,
     CK_PHI,
     CK_EQ,
     CK_REQ,
  };
private:
  const ConstraintKind Kind; //REM: Identifier for subType
protected:
  WorkListEngine * engine;
public:
  ConstraintKind getKind() const { return Kind; } //REM: Get type of constraint (LT,EQ...)
  Constraint(ConstraintKind K) : Kind(K) {}; //REM: Constructor including type of subclass
  virtual void resolve() const =0;
  virtual void print(raw_ostream &OS) const =0;
  virtual ~Constraint() {}
};

////////////////////////////////////////////////////////////////////////////////
// Constraint types declaration
class LT : public Constraint {
  StrictRelations::Variable * const left, * const right;
public:
  LT(WorkListEngine* W, StrictRelations::Variable* L, // REM: Adding type definition during creation
          StrictRelations::Variable* R) : Constraint(CK_LT), left(L), right(R) { engine = W; }; 
  void resolve() const override;
  void print(raw_ostream &OS) const override;
  static bool classof(const Constraint *C) { //REM: Return type of constraint
    return C->getKind() == CK_LT;
  }

  StrictRelations::Variable* getLeftSide() const  { //REM: Return left side of constraint
    return left;
  }

  StrictRelations::Variable* getRightSide() const  { //REM: Return Right side of constraint
    return right;
  }
};

class LE : public Constraint {
  StrictRelations::Variable * const left, * const right;
public:
  LE(WorkListEngine* W, StrictRelations::Variable* L,
          StrictRelations::Variable* R) : Constraint(CK_LE), left(L), right(R) { engine = W; };
  void resolve() const override;
  void print(raw_ostream &OS) const override;
  static bool classof(const Constraint *C) { //REM: Return type of constraint
    return C->getKind() == CK_LT;
  }

  StrictRelations::Variable* getLeftSide() const  { //REM: Return left side of constraint
    return left;
  }

  StrictRelations::Variable* getRightSide() const  { //REM: Return Right side of constraint
    return right;
  }
};

class REQ : public Constraint {
  StrictRelations::Variable * const left, * const right;
public:
  REQ(WorkListEngine* W, StrictRelations::Variable* L,
          StrictRelations::Variable* R) : Constraint(CK_REQ), left(L), right(R) { engine = W; };
  void resolve() const override;
  void print(raw_ostream &OS) const override;
  static bool classof(const Constraint *C) { //REM: Return type of constraint
    return C->getKind() == CK_LT;
  }
  StrictRelations::Variable* getLeftSide() const  { //REM: Return left side of constraint
    return left;
  }

  StrictRelations::Variable* getRightSide() const  { //REM: Return Right side of constraint
    return right;
  }
};

class EQ : public Constraint {
  StrictRelations::Variable * const left, * const right;
public:
  EQ(WorkListEngine* W, StrictRelations::Variable* L,
          StrictRelations::Variable* R) : Constraint(CK_EQ), left(L), right(R) { engine = W; };
  void resolve() const override;
  void print(raw_ostream &OS) const override;
  static bool classof(const Constraint *C) { //REM: Return type of constraint
    return C->getKind() == CK_LT;
  }
  StrictRelations::Variable* getLeftSide() const  { //REM: Return left side of constraint
    return left;
  }

  StrictRelations::Variable* getRightSide() const  { //REM: Return Right side of constraint
    return right;
  }
};

class PHI : public Constraint {
  StrictRelations::Variable * const left;
  std::unordered_set<StrictRelations::Variable*> operands;
public:
  PHI(WorkListEngine* W, StrictRelations::Variable* L,
                          std::unordered_set<StrictRelations::Variable*> Operands)
                          : Constraint(CK_PHI), left(L), operands(Operands) { engine = W; };
  void resolve() const override;
  void print(raw_ostream &OS) const override;
  static bool classof(const Constraint *C) { //REM: Return type of constraint
    return C->getKind() == CK_LT;
  }
  StrictRelations::Variable* getLeftSide() const  {
    return left;
  }

  std::unordered_set<StrictRelations::Variable*> getRightSide() const {
    return operands;
  }
};

////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// LessThan Graph and Nodes declaration

class TGraph;

class TNode {
private:
  std::unordered_set<TNode*> lessThanSet;
  std::unordered_set<TNode*> greaterThanSet;
  StrictRelations::Variable* variable;
public:
  TNode(StrictRelations::Variable*);
  ~TNode();
  
  StringRef getName(){
    return (variable->v->hasName()? variable->v->getName() : "");
  }

  const Value* getValue(){
    return variable->v;
  }

  std::unordered_set<TNode*> getSetLT();
  bool hasLT(TNode* succ);
  std::unordered_set<TNode*> getSetGT();
  bool hasGT(TNode* pred);
  void addEdgeTo(TNode *dst);
  void cloneSet(TNode* from, raw_ostream &OS);
  void clonePSet(TNode* from, raw_ostream &OS);
  void intersection(std::unordered_set<StrictRelations::Variable*> phiNode, TGraph *T,raw_ostream &OS);

};
  



class TGraph{
private:
  std::set<TNode*> nodes; 
  std::map<StrictRelations::Variable*, TNode* > nodeMap;
public:
  typedef typename std::set<TNode*>::iterator iterator;
  iterator begin();
  iterator end();

  ~TGraph(); 
  void constructFromResult(std::unordered_map<const Value*, StrictRelations::Variable*> variables, raw_ostream &OS);
  void countCC(std::map<unsigned int, std::set<TNode*> > &,raw_ostream &OS);        
  TNode* addNode(StrictRelations::Variable* element); 
  void removeNode(TNode* target);
  void addEdge(TNode* src, TNode* dst);
  void removeEdge(TNode* src, TNode* dst);

  TNode* findNode(StrictRelations::Variable* element); //Return the pointer to the node or NULL if it is not in the graph
  TNode* findNode(TNode* node); //Return the pointer to the node or NULL if it is not in the graph

  std::set<TNode*> findNodes(std::set<StrictRelations::Variable*> elements);
  void printGraph(raw_ostream &OS);        
  void toDot(raw_ostream &OS);
  void dfsVisit(TNode *u, std::set<TNode *> &visitedNodes);       
  void checkCC(std::map<unsigned int, std::set<TNode*> > ConnectedComponets, raw_ostream &OS);
  inline int getNumNodes() { return nodes.size();}

};

////////////////////////////////////////////////////////////////////////////////
// DotFile class declaration --> Export graph as a .dot file

class dotFile {
private:
  std::stringstream dot;
  std::string FunctionName;
  std::string dotName;
public:
  dotFile(std::string);
  void insertLine(std::string);
  void insertEdge(std::string, std::string);
  void toFile();
  void addVertex(const Value*);
  void addEdge(std::string, std::string);
  std::string requireName(const Value *);
  std::string recreateName(const Instruction*);

};



}


#endif
