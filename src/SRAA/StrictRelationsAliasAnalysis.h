#ifndef __StrictRelationsAliasAnalysis_H__
#define __StrictRelationsAliasAnalysis_H__

#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Pass.h"

#include "../RangeAnalysis/RangeAnalysis.h"

#include <queue>
#include <set>
#include <map>
#include <utility>
#include <vector>

#include <sstream> //junio
#include <fstream> //junio
#include <list> // junio


typedef InterProceduralRA<Cousot> InterProceduralRACousot;

namespace {

//Forward declarations
class WorkListEngine;
class Constraint;
class DepGraph;

/// Representation of types as sequences of primitive values (now bits!)
class Primitives {
  public:
  //Holds a Primitive Layout for a determined Type
  struct PrimitiveLayout {
    Type * type;
    std::vector<int> layout;
    PrimitiveLayout(Type* ty, std::vector<int> lay) {
      type = ty;
      layout = lay;
    }
  };
  struct NumPrimitive {
    Type * type;
    int num;
    NumPrimitive(Type* ty, int n) {
      type = ty;
      num = n;
    }
  };
  std::vector<PrimitiveLayout*> PrimitiveLayouts;
  std::vector<NumPrimitive*> NumPrimitives;
  std::vector<int> getPrimitiveLayout(Type* type);
  int getNumPrimitives(Type* type);
  llvm::Type* getTypeInside(Type* type, int i);
  int getSumBehind(std::vector<int> v, unsigned int i);
};

class StrictRelations : public ModulePass, public AliasAnalysis {

public:
  static char ID; // Class identification, replacement for typeinfo
  StrictRelations() : ModulePass(ID){}

  /// getAdjustedAnalysisPointer - This method is used when a pass implements
  /// an analysis interface through multiple inheritance.  If needed, it
  /// should override this to adjust the this pointer as needed for the
  /// specified pass info.
  virtual void *getAdjustedAnalysisPointer(AnalysisID PI) override {
    if (PI == &AliasAnalysis::ID)
      return (AliasAnalysis*)this;
    return this;
  }

//junio: added a variable to save the funcion name inside the struct Variable

  struct Variable {
    const Value* v;
    std::string funName; //junio
    std::set<Variable*> LT;
    std::set<Variable*> GT;
    std::set<Variable*> SA; //// REM: Loop Solve
    std::set<Constraint*> constraints;
    Variable(const Value* V) : v(V) { }
    void printStrictRelations(raw_ostream &OS);
  };

  Variable* getVariable(Value * V) {
    if(variables.count(V)) return variables.at(V);
    else return NULL;
  }
  void printAllStrictRelations(raw_ostream &OS);
  void ___countGraphElements(raw_ostream &OS); // junio (Nao movida)
  std::map<const Value*, Variable*> getResult(){
    return variables;
  }


private:
  InterProceduralRACousot *RA;
  std::map<const Value*, Variable*> variables;
  WorkListEngine* wle;
  DepGraph* dg;
  
  // Function that collects comparisons from the instructions in the module
  void collectConstraintsFromModule(Module &M);
  Range processGEP(const Value*, const Use*, const Use*);
  void buildDepGraph(Module &M);
  enum CompareResult {L, G, E, N};
  CompareResult compareValues(const Value*, const Value*);
  CompareResult compareGEPs(const GetElementPtrInst*, const GetElementPtrInst*);
  void collectConstraintsFromDepGraph();
      
  void getAnalysisUsage(AnalysisUsage &AU) const override;
  bool runOnModule(Module &M) override;
  AliasResult alias(const MemoryLocation &LocA,
                              const MemoryLocation &LocB) override;

  static Primitives P;
};

//Worklist engine declarations
class Constraint {
public:
  enum ConstraintKind { // junio // Discriminator for LLVM-style RTTI - users dyn_cast isa
     CK_LT,
     CK_LE,
     CK_PHI,
     CK_EQ,
     CK_REQ,
  };
protected:
  WorkListEngine * engine;
private:
  const ConstraintKind Kind;
public:
  ConstraintKind getKind() const { return Kind; }
  Constraint(ConstraintKind K) : Kind(K) {};

  virtual void resolve() const =0;
  virtual void print(raw_ostream &OS) const =0;
  virtual ~Constraint() {}

};

class WorkListEngine {
public:
  void solve();
  void add(Constraint*, bool isnew = true);
  //WorkListEngine is in charge of constraint deletion
  ~WorkListEngine();
  void printConstraints(raw_ostream &OS);
  std::map<const Constraint*, bool> getConstraints() { return constraints; }
  int getNumConstraints() { return constraints.size(); }
private:
  std::queue<const Constraint*> worklist;
  std::map<const Constraint*, bool> constraints;
};

// Constraint definitions
class LT : public Constraint {
  StrictRelations::Variable * const left, * const right;
public:
  LT(WorkListEngine* W, StrictRelations::Variable* L,
          StrictRelations::Variable* R) : Constraint(CK_LT), left(L), right(R) { engine = W; };
  void resolve() const override;
  void print(raw_ostream &OS) const override;

  
  StrictRelations::Variable* getLeftSide() const  {
    return left;
  }

  StrictRelations::Variable* getRightSide() const  {
    return right;
  }

  static bool classof(const Constraint *C) {
    return C->getKind() == CK_LT;
  }



};

class LE : public Constraint {
  StrictRelations::Variable * const left, * const right;
public:
  LE(WorkListEngine* W, StrictRelations::Variable* L,
          StrictRelations::Variable* R) : Constraint(CK_LE), left(L), right(R) { engine = W; };
  void resolve() const override;
  void print(raw_ostream &OS) const override;


StrictRelations::Variable* getLeftSide() const  {
    return left;
  }

  StrictRelations::Variable* getRightSide() const  {
    return right;
  }


  static bool classof(const Constraint *C) {
    return C->getKind() == CK_LE;
  }
};

class REQ : public Constraint {
  StrictRelations::Variable * const left, * const right;
public:
  REQ(WorkListEngine* W, StrictRelations::Variable* L,
          StrictRelations::Variable* R) : Constraint(CK_REQ), left(L), right(R)  { engine = W; };
  void resolve() const override;
  void print(raw_ostream &OS) const override;

StrictRelations::Variable* getLeftSide() const  {
    return left;
  }

  StrictRelations::Variable* getRightSide() const  {
    return right;
  }


  static bool classof(const Constraint *C) {
    return C->getKind() == CK_REQ;
  }
};

class EQ : public Constraint {
  StrictRelations::Variable * const left, * const right;
public:
  EQ(WorkListEngine* W, StrictRelations::Variable* L,
          StrictRelations::Variable* R) : Constraint(CK_EQ), left(L), right(R) { engine = W; };
  void resolve() const override;
  void print(raw_ostream &OS) const override;

StrictRelations::Variable* getLeftSide() const  {
    return left;
  }

  StrictRelations::Variable* getRightSide() const  {
    return right;
  }


  static bool classof(const Constraint *C) {
    return C->getKind() == CK_EQ;
  }
};

class PHI : public Constraint {
  StrictRelations::Variable * const left;
  std::set<StrictRelations::Variable*> operands;
public:
  PHI(WorkListEngine* W, StrictRelations::Variable* L,
                          std::set<StrictRelations::Variable*> Operands)
                          : Constraint(CK_PHI), left(L), operands(Operands) { engine = W; };
  void resolve() const override;
  void print(raw_ostream &OS) const override;

  StrictRelations::Variable* getLeftSide() const  {
    return left;
  }

  std::set<StrictRelations::Variable*> getRightSide() const {
    return operands;
  }

  static bool classof(const Constraint *C) {
    return C->getKind() == CK_PHI;
  }
};

//Dependance graph declarations
class DepGraph {
public:
  void addVariable(StrictRelations::Variable*);
  // In constraints x = y is necessary to coalesce the nodes
  void coalesce (StrictRelations::Variable*, StrictRelations::Variable*);
  //In the case of a GEP
  void addEdge(StrictRelations::Variable*, StrictRelations::Variable*, Range,
                                                      const GetElementPtrInst*);

  // Forward declaration
  struct DepEdge;

  enum DepOp { Addition, Subtraction, Multiplication };

  struct DepNode {
    std::set<StrictRelations::Variable*> variables;
    std::set<DepEdge*> edges;
    std::set<DepEdge*> inedges;
  };

  struct DepEdge {
    DepNode* target;
    DepOp sign;
    DepOp operation;
    DepNode* operand;
    // GAMBIARRA!!
    Range range;
    const GetElementPtrInst* gep;
  };
private:

  std::map<StrictRelations::Variable*, DepNode*> nodes;

public:
  std::map<StrictRelations::Variable*, DepNode*>::iterator
                                            begin() { return nodes.begin(); }
  std::map<StrictRelations::Variable*, DepNode*>::iterator
                                            end() { return nodes.end(); }

};



/* TNode and TGraph class */




class TGraph;

class TNode {
private:
  std::set<TNode*> lessThanSet;
  std::set<TNode*> greaterThanSet;
  StrictRelations::Variable* variable;
public:
  TNode(StrictRelations::Variable*);
  ~TNode();
  
  StringRef getName(){
    return (variable->v->hasName()? variable->v->getName() : "");
  }

  const Value* getValue(){
    return variable->v;
  }

  std::set<TNode*> getSetLT();
  bool hasLT(TNode* succ);

  std::set<TNode*> getSetGT();
  bool hasGT(TNode* pred);

  void addEdgeTo(TNode *dst);

  void cloneSet(TNode* from, raw_ostream &OS);

  void clonePSet(TNode* from, raw_ostream &OS);

  void intersection(std::set<StrictRelations::Variable*> phiNode, TGraph *T,raw_ostream &OS);

};
  
class TGraph{
private:
  std::set<TNode*> nodes; 
  std::map<StrictRelations::Variable*, TNode* > nodeMap;
public:
  typedef typename std::set<TNode*>::iterator iterator;

  iterator begin();
  iterator end();

        
  ~TGraph(); //Destructor - Free adjacent matrix's memory

  void construct(WorkListEngine*, raw_ostream &);

  void constructFromResult(std::map<const Value*, StrictRelations::Variable*> variables, raw_ostream &OS);

  void countCC(std::map<unsigned int, std::set<TNode*> > &,raw_ostream &OS);

        
  TNode* addNode(StrictRelations::Variable* element); //Add an instruction into Dependence Graph

  void removeNode(TNode* target);

  void addEdge(TNode* src, TNode* dst);
  void removeEdge(TNode* src, TNode* dst);

  TNode* findNode(StrictRelations::Variable* element); //Return the pointer to the node or NULL if it is not in the graph
  TNode* findNode(TNode* node); //Return the pointer to the node or NULL if it is not in the graph

  std::set<TNode*> findNodes(std::set<StrictRelations::Variable*> elements);

  void printGraph(raw_ostream &OS);
        
  void toDot(raw_ostream &OS);

  void dfsVisit(TNode *u, std::set<TNode *> &visitedNodes); //Used by findConnectingSubgraph() method
        
  void dfsVisitBack(TNode * u, std::set<TNode *> &visitedNodes); //Used

  void checkCC(std::map<unsigned int, std::set<TNode*> > ConnectedComponets, raw_ostream &OS);


  inline int getNumNodes() { return nodes.size();}

};



class dotFile {
private:
  std::stringstream dot;
  std::string FunctionName;
  std::string dotName;
public:
  dotFile(std::string);
  void insertLine(std::string);
  void insertEdge(std::string, std::string);
  void toFile();
  void addVertex(const Value*);
  void addEdge(std::string, std::string);
  std::string requireName(const Value *);
  std::string recreateName(const Instruction*);

};





}




#endif
